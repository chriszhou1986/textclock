package com.stylingandroid.textclock;

import android.os.Handler;
import android.service.dreams.DreamService;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Copyright 2013 Mark Allison
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TextClockDaydream extends DreamService
{
    private static final String TAG = "TextClockDaydream";
    private static final DateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.SSS" );
    private final Runnable timerTask = new Runnable()
    {
        @Override
        public void run()
        {
            scheduleTimer();
        }
    };

    private Handler handler = new Handler();
    private TextView hours;
    private TextView tens;
    private TextView minutes;

    @Override
    public void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        setInteractive( false );
        setFullscreen( true );
        setScreenBright( false );
        setContentView( R.layout.daydream );
        hours = (TextView) findViewById( R.id.hours );
        tens = (TextView) findViewById( R.id.tens );
        minutes = (TextView) findViewById( R.id.minutes );
    }

    @Override
    public void onDreamingStarted()
    {
        super.onDreamingStarted();
        scheduleTimer();
    }

    @Override
    public void onDreamingStopped()
    {
        super.onDreamingStopped();
        handler.removeCallbacks( timerTask );
    }

    private void scheduleTimer()
    {
        Calendar date = Calendar.getInstance();
        updateTime( date );
        date.set( Calendar.SECOND, 0 );
        date.set( Calendar.MILLISECOND, 0 );
        date.add( Calendar.MINUTE, 1 );
        Log.d( TAG, String.format( "Next update: %s", dateFormat.format( date.getTime() ) ) );
        handler.postDelayed( timerTask, date.getTimeInMillis() - System.currentTimeMillis() );
    }

    private void updateTime( Calendar date )
    {
        String[] words = TimeToWords.timeToWords( date );

        hours.setText( words[0] );
        tens.setText( words[1] );
        if ( words.length == 2 )
        {
            minutes.setVisibility( View.INVISIBLE );
        }
        else
        {
            minutes.setVisibility( View.VISIBLE );
            minutes.setText( words[2] );
        }
    }
}
