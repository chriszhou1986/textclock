package com.stylingandroid.textclock;

import java.util.Calendar;

/**
 * Copyright 2013 Mark Allison
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TimeToWords
{
    public static final String[] UNITS = { "zero", "one", "two", "three", "four", "five",
            "six", "seven", "eight", "nine" };
    public static final String[] TENS = { "zero", "ten", "twenty", "thirty", "forty", "fifty",
            "sixty", "seventy", "eighty", "ninety" };
    public static final String[] TEENS = { "ten", "eleven", "twelve", "thirteen", "fourteen",
            "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
    private static final StringBuilder builder = new StringBuilder();

    public synchronized static String[] timeToWords( Calendar date )
    {
        builder.setLength( 0 );
        int h = date.get( Calendar.HOUR_OF_DAY );
        int m = date.get( Calendar.MINUTE );
        if ( h == 0 )
        {
            builder.append( "midnight" );
        }
        else
        {
            toWords( h, builder, false, " " );
        }
        if ( m == 0 )
        {
            if ( h > 0 )
            {
                builder.append( ":o'clock" );
            }
        }
        else
        {
            builder.append( ":" );
            toWords( m, builder, true, ":" );
        }
        return builder.toString().split( ":" );
    }

    private static void toWords( final int number, StringBuilder sb, boolean leadingZero, String separator )
    {
        int num = number;
        int tens = num / 10;
        if ( leadingZero || tens > 0 )
        {
            if ( tens == 1 )
            {
                sb.append( TEENS[num - 10] );
                num = 0;
            }
            else
            {
                sb.append( TENS[tens] );
            }
        }
        int units = num % 10;
        if ( units > 0 )
        {
            if ( sb.length() > 0 )
            {
                sb.append( separator );
            }
            sb.append( UNITS[units] );
        }
    }
}
